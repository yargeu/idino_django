from django.shortcuts import render
from .name_list_gen import firstname_list, lastname_list, email_domain
from .models import TestUser, Score
from django.http import HttpResponse
import random
from django.db.models import Q, Max, Min, Avg, Sum, Count

def dummy_create(req):
    try:
        create_count = req.GET['n']
    except:
        return render(req, 'ex/create.html', context={'result' : '데이터 생성 실패 : n을 입력해 주세요.'})
    create_datas = []
    for i in range(int(create_count)):
        name = firstname_list[random.randint(0, len(firstname_list)-1)]
        last = lastname_list[random.randint(0, len(lastname_list)-1)]
        create_data = TestUser(
            username= name,
            first_name= name,
            last_name= last,
            email= name + email_domain[random.randint(0, len(email_domain)-1)]
            )
        create_datas.append(create_data)
    TestUser.objects.bulk_create(create_datas)
    result = {'result' : '데이터 생성 성공!' + create_count}
    return render(req, 'ex/create.html', context=result)

sihum_list = ['중간고사', '기말고사', '모의고사', '본고사']

def score_test_data(req):
    try:
        create_count = req.GET['n']
    except:
        return render(req, 'ex/create.html', context={'result' : '데이터 생성 실패 : n을 입력해 주세요.'})
    user_all = TestUser.objects.all()
    print(user_all[105])
    max_id = user_all.aggregate(max_id=Max('id'))['max_id'] #{'max_id' : 106 }
    score_create_list = []
    for _ in range(int(create_count)):
        user = user_all[random.randint(1, max_id-1)]
        math = random.randint(1, 100)
        kor = random.randint(1, 100)
        eng = random.randint(1, 100)
        sihum = sihum_list[random.randint(0, len(sihum_list)-1)]
        s = Score(
            user= user,
            math=math,
            kor=kor,
            eng=eng,
            sihum=sihum
        )
        score_create_list.append(s)
    Score.objects.bulk_create(score_create_list)
    
    return render(req, 'ex/create.html', context={'result' : '데이터 생성 성공! '+ create_count})


def score_agg(req):
    try:
        subject = req.GET['sub']
        #agg = req.GET['agg']
    except:
        return HttpResponse("입력값을 확인하세요.")
    result = Score.objects.all().aggregate(n=Count('id'), max_id=Max('id'),avg = Avg(subject), max = Max(subject), min = Min(subject), sum=Sum(subject))
    return HttpResponse(str(result))


def all_view(req):
    result = TestUser.objects.all() #쿼리셋을 출력한다. [쿼리1, 쿼리2, .....]
    result = result.values() #[dict1, dict2, .....]
    sql_q = result.query
    return render(req, 'ex/all.html', context={'result' : result, 'sql' : sql_q})

def test_q(req):
    result = TestUser.objects.filter(Q(last_name__startswith='L') | Q(username__endswith= 'a'))
    result = result.values()
    sql_q = result.query
    return render(req, 'ex/test.html', context={'result' : result, 'sql' : sql_q})


def join_data(req):
    result = TestUser.objects.all().prefetch_related('score')
    print(result[20].score.filter(eng__lt = 4))
    sql_q = result.query
    return render(req, 'ex/test.html', context={'result' : result.values(), 'sql' : sql_q})
    
    '''
    result = Score.objects.filter(id__lt=21).select_related('user').values(
        'id', 'math', 'kor', 'eng', 'sihum', 
        'user__username', 'user__last_name', 'user__email'
        )
    sql_q = result.query
    return render(req, 'ex/test.html', context={'result' : result, 'sql' : sql_q})
    '''