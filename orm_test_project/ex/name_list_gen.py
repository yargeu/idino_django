from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
name_file_path = str(BASE_DIR / 'name/')

get_name = lambda s :''.join([i if 62< ord(i) < 91 or 96< ord(i) < 123 else '' for i in s]).title()


firstname_list = list(map(get_name, open(name_file_path+'/name.txt', 'rt', encoding='UTF8').readlines()))
lastname_list = list(map(get_name, open(name_file_path+'/last.txt', 'rt', encoding='UTF8').readlines()))
email_domain = ['@gmail.com', '@naver.com', '@hanmail.net', '@hotmail.com', '@yahoo.co.kr', '@gitlab.com']