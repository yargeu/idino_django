from django.urls import path
from .views import *

urlpatterns = [
    path('dummy-create/', dummy_create, name='create'),
    path('all/', all_view, name='all'),
    path('test/', test_q, name='test'),
    path('score-create/', score_test_data, name='score_create'),
    path('score-agg/', score_agg, name='score_agg'),
    path('score-join/', join_data),
]