from django.db import models

class TestUser(models.Model):
    username = models.CharField(max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField()

    def __str__(self):
        return self.username

class Score(models.Model):
    user = models.ForeignKey(TestUser, on_delete=models.CASCADE, related_name='score')
    kor = models.IntegerField()
    math = models.IntegerField()
    eng = models.IntegerField()
    sihum = models.CharField(max_length=30)

    def __str__(self):
        return self.user.username