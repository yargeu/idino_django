from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('post-all/', post, name = 'post_all')
]