from django.shortcuts import render
from .models import Category, Post

def index(req):
    posts = Post.objects.order_by("-createDate")[:4]
    context = {
        'posts' : posts
    }
    return render(req, 'index.html', context=context)

def post(req):
    posts = Post.objects.order_by("-createDate")[:3]
    return render(req, 'post_all.html', context={'posts' : posts})